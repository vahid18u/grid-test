import React from "react";
import "./list-style.css";
const List = props => {
  const colorsArray = [
    "Green",
    "Blue",
    "Red",
    "Aqua",
    "Yellow",
    "Orange",
    "Purple",
    "Pink",
    "Brown",
    "Black",
    "Gray",
    "Violet",
    "Bisque",
    "Blueviolet",
    "Cornflowerblue",
    "Darkblue"
  ];

  return (
    <ul>
      {colorsArray.map((item, index) => (
        <li key={index}>
          <input
            type="checkbox"
            name={item}
            value={item}
            onClick={props.handleClick}
          ></input>
          <label htmlFor={item}>{item}</label>
        </li>
      ))}
    </ul>
  );
};
export default List;
