import React from "react";
import "./grid-style.css";
const Grid = props => {
  const getManipulatedArray = input => {
    let result = [];
    let k = 0;
    for (let i = 12; i >= 0; i -= 4) {
      for (let j = 1; j <= 4; j++) {
        result[i + j] = input[k++];
      }
    }
    return result;
  };

  return (
    <>
      <div className="grid-container">
        {getManipulatedArray(props.givenGrid).map((item, index) => (
          <div className={`grid-item ${item}`} key={index}>
            {item === "empty" && <span>empty</span>}
          </div>
        ))}
      </div>
    </>
  );
};
export default Grid;
