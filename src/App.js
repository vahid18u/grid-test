import React, { useState, useEffect } from "react";
import "./App.css";
import Grid from "./components/grid/grid-component";
import List from "./components/list/list-component";
function App() {
  const [grid, setGrid] = useState([]);
  useEffect(() => {
    const initializedGrid = [];
    for (let i = 0; i < 16; i++) {
      initializedGrid.push("empty");
    }
    setGrid(initializedGrid);
  }, []);
  const handleClick = event => {
    if (event.target.checked) {
      addToGrid(event.target.name);
    } else {
      removeFromGrid(event.target.name);
    }
  };

  const addToGrid = color => {
    const newGrid = [...grid];
    for (let i = 0; i < 16; i++) {
      if (newGrid[i] !== "empty") continue;
      else {
        newGrid[i] = color;
        break;
      }
    }
    setGrid(newGrid);
  };
  const removeFromGrid = color => {
    const newGrid = [...grid];

    const index = newGrid.indexOf(color);
    if (index > -1) {
      newGrid.splice(index, 1);
    }
    newGrid.push("empty");
    setGrid(newGrid);
  };

  return (
    <>
      <Grid givenGrid={grid} />
      <List handleClick={handleClick} />
    </>
  );
}

export default App;
